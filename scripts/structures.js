define(function() {
    function Structure(x, y, width, height, name, description, primaryProperty, secondaryProperty) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.name = name;
        this.description = description;
        this.primaryProperty = primaryProperty;
        this.secondaryProperty = secondaryProperty;
    }

    return Structure;
});