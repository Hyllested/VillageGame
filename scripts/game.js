define(function(require) {
    var Structure = require('./structures');
    var Villager = require('./villager');
    var Victor = require('../victor');

    // Create the canvas
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    canvas.width = 1280;
    canvas.height = 720;
    document.body.appendChild(canvas);

    //Used for showing information about structures clicked
    var clickedItem = "";
    var clickedDescription = "";
    var clickedPriProperty = "";
    var clickedSecProperty = "";
    var clickedItemFlag = false;

    //Used for showing information about villagers
    var clickedVillager = "";
    var clickedVillagerAge = "";
    var clickedVillagerDeathAge = "";
    var clickedVillagerFlag = false;

    // Array for holding images of villagers
    var villagerImagesArray = [];

    // Array for holding villager image sources
    var villagerImageSources = [
        "images/bubbiVillager.png",
        "images/villagerTwo.png",
        "images/villagerThree.png",
        "images/villagerFour.png",
        "images/villagerFive.png",
        "images/villagerSix.png",
        "images/villagerSeven.png",
        "images/villagerEight.png",
        "images/villagerNine.png",
        "images/villagerTen.png"
    ];

    // Load HUD backgound image
    var hudReady = false;
    var hudImage = new Image();
    hudImage.onload = function() {
        hudReady = true;
    };
    hudImage.src = "images/HUD.png";
    
    // Load coconut palm
    var coconutPalmReady = false;
    var coconutPalmImage = new Image();
    coconutPalmImage.onload = function() {
        coconutPalmReady = true;
    };
    coconutPalmImage.src = "images/coconutPalm.png";

    //Array for holding all villagers in game 
    var villagers = [];

    //Counter used to control how long villagers walk before maybe changing direction
    var runCounter = 0;

    // Selected game object
    var selectedObject;

    // Event listeners
    canvas.addEventListener("click", canvasClicked);
    canvas.addEventListener("contextmenu", canvasRightClicked);

    function canvasRightClicked(e) {
        e.preventDefault();
        // Check if a villager is selected
        if (villagerIsSelected()) {
            var destination = new Victor(e.pageX, e.pageY);
            selectedObject.startWalking(destination);
        }
    }

    function canvasClicked(e) {
        // If keg is cliked
        if (keg.x < e.pageX && e.pageX < keg.x + keg.width) {
            if (keg.y < e.pageY && e.pageY < keg.y + keg.height) {
                clickedItem = keg.name;
                clickedItemDescription = keg.description;
                clickedItemPriProperty = keg.primaryProperty;
                clickedItemSecProperty = keg.secondaryProperty;
                clickedItemFlag = true;
            } else {
                clickedItemFlag = false;
            }
        }
        else {
            clickedItemFlag = false;
        }

        // If villager clicked
        try {
            villagers.forEach(function(villager) {
                if (villager.x < e.pageX && e.pageX < villager.x + villager.width) {
                    if (villager.y < e.pageY && e.pageY < villager.y + villager.height) {
                        villager.setState(villager.states.SELECTED);
                        selectedObject = villager;
                        clickedVillager = villager.name;
                        //clickedVillagerAge = villager.getAge();
                        clickedVillagerDeathAge = villager.getDeathAge();
                        clickedVillagerFlag = true;
                        throw new error();
                    }
                }
                else {
                    clickedVillagerFlag = false;
                    selectedObject = undefined;
                }
            })
        } catch (error) {

        }

        if (!clickedItem) {
            clickedItem = "";
            clickedItemDescription = "";
            clickedItemPriProperty = "";
            clickedItemSecProperty = "";
            clickedItemFlag = false;
        }
        else if (!clickedVillagerFlag) {
            clickedVillager = "";
            clickedVillagerAge = "";
            clickedVillagerFlag = false;

        };
    }

    // Backgound image
    var bgReady = false;
    var bgImage = new Image();
    bgImage.onload = function() {
        bgReady = true;
    };
    bgImage.src = "images/background.png";

    //Get a random birth age between min and max
    var randomAge = function(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    //Get a random death age between min and max   
    var randomDeathAge = function(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
    //Function to age villagers 
    var ageVillagers = function() {
        refreshRate = 2;  // 5 Seconds
        villagers.forEach(function(villager) {
            villager.setAge(villager.getAge() + 1);
        });
        setTimeout(ageVillagers, refreshRate * 1000);
    }

    ageVillagers(); // execute function

    //Function to get villager information
    var getVillagerInformation = function(villager) {
        ctx.fillStyle = "rgb(0, 0, 0)";
        ctx.font = "15px pixelMusketeer";
        ctx.textAlign = "left";
        ctx.textBaseline = "top";
        ctx.fillText("Name: " + villager.getName(), 72, 70);
        ctx.fillText("Age: " + villager.getAge(), 72, 82);
        ctx.fillText("Death Age: " + villager.getDeathAge(), 72, 94);

    }

    //Kill villagers
    var killVillagers = function(villagerArray) {
        villagers.forEach(function(villager, index) {
            var currentAge = villager.getAge();
            var deathAge = villager.getDeathAge();

            if (currentAge > deathAge) {
                villagerArray.splice(index, 1);
                villagerImagesArray.splice(index, 1);
            }
        })
    }
    //Move villagers at random until clicked
    //TODO: Change so that if villager is at the boarder of canvas, he should random between directions away from the border 
    var idleWalk = function() {
        runCounter++;
        if (runCounter >= 200) {
            var resetflag = true;
            runCounter = 0;
        }
        else {
            resetflag = false;
        }
        villagers.forEach(function(villager) {
            if (villager.state === villager.states.IDLE) {
                if (resetflag) {
                    villager.direction = getRandomDirection();
                }
                villager.x = villager.x + villager.direction.x;
                villager.y = villager.y + villager.direction.y;
            }

        })
    }

    function getRandomDirection() {
        var radians = Math.random() * Math.PI * 2;
        return new Victor(Math.cos(radians), Math.sin(radians));
    }


    function walk() {
        villagers.forEach(function(villager) {
            if (villager.state === villager.states.WALKING) {
                villager.walkTowardsDestination();
            }
            //TODO: Insert Martin Myrup collission detection magic
        });
    }

    function villagerIsSelected() {
        return (selectedObject && villagers.indexOf(selectedObject > 0));
    }

    var kegReady = false;
    var kegImage = new Image();
    kegImage.onload = function() {
        kegReady = true;
    };
    kegImage.src = "images/keg.png";
    var keg = new Structure(430, 440, 76, 46, "A keg of rum", "Contains rum!", "Gives unlimted power", "+10 in awesomeness");
    
    var coconutPalm = new Structure(1132, 208, 148, 162); 

    var render = function() {
        if (bgReady) {
            ctx.drawImage(bgImage, 0, 0, 1280, 720);
        }
        if (kegReady) {
            ctx.drawImage(kegImage, keg.x, keg.y, 76, 46);
        }
        if (coconutPalmReady) {
            ctx.drawImage(coconutPalmImage, coconutPalm.x, coconutPalm.y, 148, 162);
        }

        for (var i = 0; i < villagers.length; i++) {
            var image = villagerImagesArray[i];
            if (image) {
                ctx.drawImage(image.villagerImage, villagers[i].x, villagers[i].y, 64, 64);
            }
        }

        // Render HUD
        if (clickedItemFlag) {
            ctx.drawImage(hudImage, 20, 20, 312, 280);
            ctx.fillStyle = "rgb(0, 0, 0)";
            ctx.font = "15px pixelMusketeer";
            ctx.textAlign = "left";
            ctx.textBaseline = "top";
            ctx.fillText("Item: " + clickedItem, 72, 70);
            ctx.fillText("Description: " + clickedItemDescription, 72, 82);
            ctx.fillText(clickedItemPriProperty, 72, 94);
            ctx.fillText(clickedItemSecProperty, 72, 106);
        }
        if (typeof selectedObject === "object") {
            ctx.drawImage(hudImage, 20, 20, 312, 280);
            getVillagerInformation(selectedObject);
        }

        killVillagers(villagers);
    };

    //Initialize villagers
    var initVillagers = function(numberOfVillagers) {
        for (i = 0; i <= numberOfVillagers; i++) {
            var randomSpawn = {
                width: Math.random() * 250,
                height: Math.random() * 250,
                direction: getRandomDirection()
            };
            var age = randomAge(16, 30);
            var ageDeath = randomDeathAge(50, 70)
            var villager = new Villager("Villager" + i, canvas.width / 2 + randomSpawn.width, canvas.height / 2 + randomSpawn.height, randomSpawn.direction, "Male", age, ageDeath);
            villagers.push(villager);
        };
        initVillagerImages();
    };

    function initVillagerImages() {
        // Load villager images
        villagers.forEach(function(villager) {
            var villagerImageObject = {
                villagerImageReady: false,
                villagerImage: new Image()
            }

            villagerImageObject.villagerImageReady.onload = function() {
                villagerImageObject.villagerImageReady = true;
            };

            // Choose villager image source randomly
            var villagerImageSource = villagerImageSources[Math.floor(Math.random() * villagerImageSources.length)];
            villagerImageObject.villagerImage.src = villagerImageSource;

            villagerImagesArray.push(villagerImageObject);
        });
    }

    // Main game loop
    var main = function() {
        var now = Date.now();
        var delta = now - then;
        idleWalk();
        walk();
        render();

        then = now;

        // Request to do this again ASAP
        requestAnimationFrame(main);
    };

    initVillagers(10);

    var then = Date.now();
    main();
});