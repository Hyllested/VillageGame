define(['require', 'Victor'], function() {
    var Victor = require('Victor');
    // TODO: cleanup getters/setters to follow constructor sequence
    function Villager(name, x, y, direction, gender, age, deathAge) {
        this.name = name;
        this.gender = gender;
        this.health = 100;
        this.maxHealth = 100;

        // Constant states
        this.states = {
            IDLE: 0,
            SELECTED: 1,
            WALKING: 2
        };

        this.x = x;
        this.y = y;

        this.width = 64;
        this.height = 64;
        this.direction = new Victor(0,0);
        this.destination = new Victor(x, y);
        this.speed = 2;
        this.state = this.states.IDLE;
        this.age = age;
        this.deathAge = deathAge;

        if (this.health == 0) this.death = true;
    }

    // Get and set villager name
    Villager.prototype.getName = function() {
        return this.name;
    };

    Villager.prototype.setName = function(name) {
        this.name = name;
    };

    // Get and set villager x position
    Villager.prototype.getX = function() {
        return this.x;
    };

    Villager.prototype.setX = function(x) {
        this.x = x;
    };

    // Get and set villager y position
    Villager.prototype.getY = function() {
        return this.y;
    };

    Villager.prototype.setY = function(y) {
        this.y = y;
    };

    Villager.prototype.getDestination = function() {
        return this.destination;
    };

    Villager.prototype.setDestination = function(destination) {
        this.destination = destination;
    };

    // Get and set villager gender
    Villager.prototype.getGender = function() {
        return this.gender;
    };

    Villager.prototype.setGender = function(gender) {
        this.gender = gender;
    };

    // Get and set current villager health
    Villager.prototype.getHealth = function() {
        return this.health;
    };

    Villager.prototype.setHealth = function(health) {
        this.health = health;
    };

    // Get villager max health
    Villager.prototype.getMaxHealth = function() {
        return this.maxHealth;
    };

    // Get and set death for villager
    Villager.prototype.getDeath = function() {
        return this.death;
    }

    Villager.prototype.setDeath = function(death) {
        this.death = death;
    };

    Villager.prototype.setState = function(state) {
        this.state = state;
    }
    
    Villager.prototype.setAge = function(age){
        this.age = age;
    }
    
    Villager.prototype.getAge = function(age) {
        return this.age;
    }
    
    Villager.prototype.setDeathAge = function(deathAge){
        this.age = deathAge;
    }
    
    Villager.prototype.getDeathAge = function(deathAge) {
        return this.deathAge;
    }

    Villager.prototype.startWalking = function(destination) {
        // Correct for villager width and height to make the
        // feet of the villager hit the destination
        var position = new Victor(this.x + (this.width / 2), this.y + this.height);

        this.destination = destination;
        this.direction = position.subtract(destination).normalize();
        this.state = this.states.WALKING;
    }

    Villager.prototype.walkTowardsDestination = function() {
        this.x = this.x - this.direction.x * this.speed * 1;
        this.y = this.y - this.direction.y * this.speed * 1;
    }

    return Villager;
});